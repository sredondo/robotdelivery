CREATE DATABASE API;
CREATE SCHEMA API;
GRANT ALL PRIVILEGES ON DATABASE API TO postgres;
GRANT ALL PRIVILEGES ON SCHEMA API TO postgres;
CREATE TABLE API.ORDER (
  id SERIAL NOT NULL PRIMARY KEY,
  detail JSON NOT NULL
);


