'use strict';

var url = require('url');

var UpdateRestaurant = require('./UpdateRestaurantService');

module.exports.put = function put (req, res, next) {
  UpdateRestaurant.put(req.swagger.params, res, next);
};
