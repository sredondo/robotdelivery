'use strict';

exports.findRestaurants = function(args, res, next) {
  /**
   * Restaurants availables
   * The Restaurants endpoint returns information about the *Restaurant* restaurants offered at a given location. The response includes the display name and other details about each restaurants. 
   *
   * latitude String Latitude component of location.
   * longitude String Longitude component of location.
   * rating BigDecimal Restaurant rating (optional)
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "address" : "aeiou",
  "rating" : "aeiou",
  "commercialEmail" : "aeiou",
  "description" : "aeiou",
  "legalName" : "aeiou",
  "reviews" : [ {
    "review" : "aeiou",
    "name" : "aeiou",
    "rating" : 1.3203312761844046008263831026852130889892578125
  } ],
  "price" : 1.46581298050294517310021547018550336360931396484375,
  "name" : "aeiou",
  "logo" : "aeiou",
  "adminNumber" : "aeiou",
  "id" : "aeiou",
  "commercialName" : "aeiou",
  "meals" : [ {
    "price" : 6.02745618307040320615897144307382404804229736328125,
    "name" : "aeiou",
    "description" : "aeiou"
  } ],
  "Location" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

