'use strict';

var url = require('url');

var CreateOrder = require('./CreateOrderService');

module.exports.ordersPOST = function ordersPOST (req, res, next) {
  CreateOrder.ordersPOST(req.swagger.params, res, next);
};
