'use strict';

var url = require('url');

var RateRestaurant = require('./RateRestaurantService');

module.exports.post = function post (req, res, next) {
  RateRestaurant.post(req.swagger.params, res, next);
};
