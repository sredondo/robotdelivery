'use strict';

exports.ordersPOST = function(args, res, next) {

    global.db.api.order.save({detail: args.order.value}).then(order=>{
        //The estimate time to arrival would be calculated based
        //on user location and restaurant location (transport media being a motorcycle), this time should have in count
        // traffic at the moment
        console.log('Order created, the estimate time to arrival is: 20M' );

        global.rabbitConnection.publish('order',order);
        global.rabbitConnection.publish('orderNotify',order);

        res.end(JSON.stringify({estimateTimeToArrive:'20M'}));
    });

}

