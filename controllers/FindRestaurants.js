'use strict';

var url = require('url');

var FindRestaurants = require('./FindRestaurantsService');

module.exports.findRestaurants = function findRestaurants (req, res, next) {
  FindRestaurants.findRestaurants(req.swagger.params, res, next);
};
