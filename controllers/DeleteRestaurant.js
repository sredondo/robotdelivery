'use strict';

var url = require('url');

var DeleteRestaurant = require('./DeleteRestaurantService');

module.exports.delete = function ddelete (req, res, next) {
  DeleteRestaurant.delete(req.swagger.params, res, next);
};
