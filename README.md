### Running the server
To run the necesary service to run the api, database and rabbitmq, run:

```
sh run.sh
or if you are running windows opertaing system, use only docker-compose file to run necesary 
services
```

To run the server, run:

```
npm start
```

To view the Swagger UI interface:

```
open http://localhost:8081/docs
```

This project leverages the mega-awesome [swagger-tools](https://github.com/apigee-127/swagger-tools) middleware which does most all the work.
